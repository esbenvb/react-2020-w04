# Øvelse 1.1

## Check branchen `exercise1` ud før denne øvelse. (husk `npm install`).

Lav `CarOverview` om til at vise en liste af biler

Tip:

Lav `const car` om til et array kaldet `cars`, brug `map()` til rendering, og husk en `key` prop i hvert `<CarItem>` element.

# Øvelse 1.2

Gør det muligt at vælge en bil i `CarOverview`:

Lav en `isSelected` boolean prop på `CarItem`, som sætter en `bg=“primary”` prop på `<Card>` elmemented når denne er `true`. - Således at man visuelt kan se at en bil er valgt.

Lav en `handleSelect` metode som opdaterer `state` med den valgte bil.

Tips:

Sørg for at `CarItem` får en reference til `handleSelect` med I sin `onSelect` prop.

Sørg for at sende en `isSelected = true` prop med til den bil, hvis ID svarer til den bil som er valgt i `this.state`.

# Øvelse 2.1

## Check branchen `exercise2` ud før denne øvelse. (husk `npm install`).

Tag udgangspunkt i `CarOverview` og `CarItem` komponenterne, og lav en `TodoList` og `TodoItem` komponent, og sørg for at tilpasse data-modellen til at indeholde en todo-list.

Som data skal (en del af) denne liste kopieres ind i `TodoList` filen, i stedet for listen af biler:

https://jsonplaceholder.typicode.com/todos

Sørg for at datamodellen i komponenten overholder formatet i listen.

Sørg for at hvert item har forskellig farve alt efter om de er completed eller ej - check alle muligheder på https://react-bootstrap.github.io/components/cards/#card-styles

Omdøb `Select` knappen til `Done`, Sørg for at der kun vises en Done-knap på de items some ikke er completed. Indtil videre skal Done-knappen ikke gøre noget.

# Øvelse 2.2

Nu vil vi gerne loade listen fra URLen, frem for at have den stående i koden. Sørg for at `TodoList` komponenten loader listen når den vises.

Tips:
Læg listen i komponentens `state` frem for i en `const`.

Brug `componentDidMount` til at loade listen - hent via `fetch` APIet - husk at bruge `async`/`await`.

# Øvelse 3.1

## Check branchen `exercise3` ud før denne øvelse. (husk `npm install`).

Et tryk på `Done` knappen skal sætte en todo til at være complete.

Tip:

Opdater `state`, og brug spreads til at kopiere resten af `state` over i den nye state, sammen med den del af state som du opdaterer med `completed: true`. Kig på muligheden for at læse forrige state, inden man returnerer et object i `this.setState()`.

# Øvelse 3.2

Sørg for at sende en request til en server, når en todo bliver completed.

Opret en Requestbin her, for at mocke et REST-API.
https://requestbin.com/

Lav derefter en HTTP `PATCH`-request til en URL der ligner denne (tag udgangspunkt i den URL du får fra RequestBin):

`https://<BIN-ID>.pipedream.net/todos/<TODO-ID>`

Med en JSON-body:

```json
{ “completed”: true}
```

Læs mere her, om hvordan man laver den type requests med fetch API

https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

Tips:

Omskriv metoden som håndterer tryk på Done, så den er `async`, og lav HTTP-requesten inden `setState`, således at den lokale state først opdateres når/hvis serveren har sagt OK.

Se hvordan der logges requests på RequestBin, og hvordan todos bliver completed i brugerfladen derefter.

Hvis der sker fejl, så prøv at åbne Chrome Developer tools og kigge i Console, hvad der logges. - Der er ofte et hint.
