### MDN Javascript reference

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference

### Play around with react in a browser

https://repl.it/languages/reactjs

### React component lifecycle

https://medium.com/@saharshgoyal/react-16-new-life-cycle-methods-inside-out-fdd269c43ccd

### UI frameworks

https://medium.com/@zeolearn/6-best-reactjs-based-ui-frameworks-9c780b96236c
https://react-bootstrap.github.io/

### Examples of storybook

https://storybook.js.org/docs/examples/

### Set up storybook for react

https://storybook.js.org/docs/guides/guide-react/

### General react tips

https://americanexpress.io/clean-code-dirty-code/
https://blog.logrocket.com/5-common-practices-that-you-can-stop-doing-in-react-9e866df5d269/

### Jest mv.

https://jestjs.io/
https://reactjs.org/docs/test-renderer.html

### Redux video tutorials

https://www.youtube.com/watch?v=1w-oQ-i1XB8&list=PLAb0jL51kOuFMxWcdTpTaZAAO5pXA3cDq

### Redux devtools extension

https://www.npmjs.com/package/redux-devtools-extension

### Why use immutability in Javascript?

https://stackoverflow.com/a/34385684/1458201
